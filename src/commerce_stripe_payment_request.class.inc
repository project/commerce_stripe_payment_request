<?php

/**
 * @file
 * Contains the CommerceGooglePayInterface interface.
 */

/**
 * Interface representing Commerce Stripe Payment Request backend class.
 */
interface CommerceStripePaymentRequestInterface {

  /**
   * CommerceStripePaymentRequestInterface constructor.
   *
   * @param $order
   *   Commerce order.
   */
  public function __construct($order);

  /**
   * Make payment transaction.
   *
   * @throws Exception
   */
  public function makeTransaction();

  /**
   * Complete the order after successful transaction.
   *
   * @return array
   *   Data for JSON output to the frontend.
   */
  public function finalizeOrder();

}

/**
 * Implementation of one-time Stripe Payment Request.
 */
class CommerceStripePaymentRequest implements CommerceStripePaymentRequestInterface {

  protected $order;

  protected $context = [];

  /**
   * CommerceStripePaymentRequest constructor.
   *
   * @param $order
   *   Commerce order.
   */
  public function __construct($order) {
    $this->order = $order;
    $this->context = $this->initGlobalContext();
  }

  /**
   * Helper to get payment setting from context array.
   *
   * @param string $setting_name
   *   Setting name.
   *
   * @return mixed
   *   Requested setting.
   */
  public function getPaymentSetting($setting_name) {
    return $this->context['payment_method']['settings'][$setting_name];
  }

  /**
   * Get all required data from global scope.
   *
   * @throws Exception
   */
  protected function initGlobalContext() {
    // Include Stripe library.
    $library = libraries_get_path('stripe-php');
    require_once($library . '/init.php');

    $context = [
      'payment_request' => [],
      'payment_method' => [],
      'stripe_data' => [],
      'form_values' => [],
      'metadata' => [],
    ];

    // Parse the data from $_POST.
    $post = json_decode(file_get_contents('php://input'), TRUE);

    // Form values are passed as a serialized string.
    $form_values = [];
    parse_str($post['data']['serialized_form'], $form_values);

    if (empty($post['data']['token']) || empty($post['data']['payment_method_id']) || empty($post['data']['payment_request']) || empty($post['data']['result']) || empty($form_values)) {
      // Server will return 500 to the frontend.
      throw new Exception('Cannot initialize CommerceStripePaymentRequest class with empty or incorrect data.');
    }

    // Get stripe backend key and init it.
    $payment_method = commerce_payment_method_instance_load($post['data']['payment_method_id']);

    $context['payment_request'] = $post['data']['payment_request'];
    $context['payment_method'] = $payment_method;
    $context['stripe_data'] = $post['data']['result'];
    $context['form_values'] = $form_values;
    $context['metadata'] = ["order_id" => $this->order->order_id];

    // Allow other modules to alter data / update order before the transaction.
    drupal_alter('commerce_stripe_payment_request_stripe_context', $this->order, $context);

    // Reload the order after possible changes in alter hooks.
    $this->order = commerce_order_load($this->order->order_id);

    // Init Stripe.
    $stripe_secret_key = $payment_method['settings']['stripe_secret_key'];
    \Stripe\Stripe::setApiKey($stripe_secret_key);

    return $context;
  }

  /**
   * Make single transaction.
   *
   * @throws \Stripe\Error\Base
   */
  public function makeTransaction() {
    $order = $this->order;
    $context = $this->context;

    $order_wrapper = entity_metadata_wrapper('commerce_order', $this->order);
    $order_total_data = $order_wrapper->commerce_order_total->value();
    $payment_method_id = $context['payment_method']['instance_id'];

    try {
      // Charge the user's card immediately.
      $charge = \Stripe\Charge::create([
        // NOTE: Stripe uses the same amount encoding as Drupal
        // Commerce (1050 = 10.50).
        'amount' => $order_total_data['amount'],
        'currency' => $order_total_data['currency_code'],
        'description' => $context['payment_request']['total']['label'],
        'metadata' => $context['metadata'],
        'source' => $context['stripe_data']['token']['id'],
      ]);

      // Prepare a transaction object to log the API response.
      $transaction = commerce_payment_transaction_new('commerce_stripe_payment_request', $order->order_id);
      $transaction->instance_id = $payment_method_id;
      $transaction->amount = $order_total_data['amount'];
      $transaction->currency_code = $order_total_data['currency_code'];
      $transaction->payload[REQUEST_TIME] = $charge->getLastResponse()->json;
      $transaction->remote_id = $charge->id;
      $transaction->remote_status = $charge->status;
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = 'Stripe charge created successfully.';
      commerce_payment_transaction_save($transaction);
    }
    catch (\Stripe\Error\Base $e) {

      $transaction = commerce_payment_transaction_new('commerce_stripe_payment_request', $order->order_id);
      $transaction->instance_id = $payment_method_id;
      $transaction->amount = $order_total_data['amount'];
      $transaction->currency_code = $order_total_data['currency_code'];
      $transaction->payload[REQUEST_TIME] = $e->getJsonBody();
      $transaction->remote_status = 'error';
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = 'Could not complete Stripe charge. ' . $e->getMessage();
      commerce_payment_transaction_save($transaction);

      watchdog_exception('commerce_stripe_payment_request', $e);

      // Server will return 500 to the frontend.
      drupal_set_message(t('An error occurred processing this payment. The error is: @e', [
        '@e' => $e,
      ]), 'error');
    }
  }

  /**
   * Complete the order after successful transaction.
   */
  public function finalizeOrder() {
    commerce_order_status_update($this->order, 'checkout_complete', FALSE, FALSE);
    commerce_checkout_complete($this->order);

    $response = [
      'redirect_uri' => url(commerce_checkout_order_uri($this->order)),
    ];

    // Allows other modules to alter response data.
    drupal_alter('commerce_stripe_payment_request_payment_response', $response, $this->order);

    return $response;
  }

}
