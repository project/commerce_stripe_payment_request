<?php

/**
 * @file
 * Drupal API documentation for Commerce Stripe Payment Request module.
 */

/**
 * Allows modules to alter JS settings for clientside configuration.
 *
 * @param $js_settings_data
 *   This data will be available on the fronend as
 *   Drupal.settings.commerce_stripe_payment_request.
 *   Array with the following keys:
 *   - payment_method: payment method instance id
 *   - charge_path: path on the server to complete Stripe charge
 *   - paymentRequest: The Google/Apple/Browser Payment request button.
 *     See https://stripe.com/docs/stripe-js/elements/payment-request-button
 *
 * @param object $order
 *   Commerce order object.
 * @param array $context
 *   An array of contextual information including keys:
 *   - payment_method
 *   - pane_values
 *   - checkout_pane
 *
 */
function hook_commerce_stripe_payment_request_payment_request_alter(&$js_settings_data, $order, $context) {
  // Require billing address from Apple/Google Pay.
  // Can be used with _commerce_stripe_payment_request_customer_profile_address().
  $js_settings_data['paymentRequest']['requiredBillingContactFields'] = ['postalAddress'];
  // Require email address from Apple/Google Pay.
  $js_settings_data['paymentRequest']['requiredShippingContactFields'] = ['email'];
}

/**
 * Allows other modules to alter response data.
 *
 * @param $response
 *   An array contains response data from Stripe.
 * @param $order
 *   Commerce order object.
 */
function hook_commerce_stripe_payment_request_payment_response_alter(&$response, $order) {
  $complete_url = custom_module_commerce_checkout_complete_redirect_url($order);
  if (!empty($complete_url)) {
    $response['redirect_uri'] = $complete_url;
  }
}

/**
 * Allow other modules to update context and order before the transaction is
 * completed.
 *
 * Payment Request doesn't submit the checkout form. Instead, all form data is
 * sent via AJAX and is passed in alter hook in $context['form_values'].
 *
 * @param $order
 *   Commerce order object.
 * @param $context
 *   An array of contextual information including:
 *   - payment_request: original paymentRequest array sent to Apple/Google API
 *   - payment_method: payment method array
 *   - stripe_data: result from Stripe fronend callback with all customer data
 *   - form_values: all checkout form values passed to the backend via AJAX
 *   - metadata: any metadata to be saved in Stripe
 */
function hook_commerce_stripe_payment_request_context_alter(&$order, &$context) {
  // Create customer profile and save billing address.
  _commerce_stripe_payment_request_customer_profile_address($order, $context['stripe_data']);
}
