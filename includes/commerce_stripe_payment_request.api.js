(function ($) {
  Drupal.behaviors.exampleModule.attach = function (context, settings) {
    /**
     * 'stripepaymentrequestpaysuccess' is fired after successful payment and before
     * final redirect to checkout complete page.
     */
    $('#stripe-payment-request-button', context).on('stripePaymentRequestPaySuccess', function (ev) {
      // Data that has been sent to the backend.
      var charge_data = ev.charge_data;
      // Data that has been received from the backend.
      var server_data = ev.server_response;
    });

    /**
     * 'stripepaymentrequestpayerror' is fired in case of error either from Stripe or from Drupal backend.
     */
    $('#stripe-payment-request-button', context).on('stripePaymentRequestPayError', function (ev) {
      console.log(ev.error_message);
    });
  }

})(jQuery);
