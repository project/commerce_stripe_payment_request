(function ($) {
  /**
   * Disable the continue buttons in the checkout process once they are clicked
   * and provide a notification to the user.
   */
  Drupal.behaviors.commerceStripeWebPaymentRequest = {
    attach: function (context, settings) {
      /**
       * Detect what browser we're on.
       */
      var browser = (function(agent){
        switch(true){
          case agent.indexOf("edge") > -1: return "ie";
          case agent.indexOf("msie") > -1: return "ie";
          case agent.indexOf("opr") > -1 && !!window.opr: return "opera";
          case agent.indexOf("chrome") > -1 && !!window.chrome: return "chrome";
          case agent.indexOf("trident") > -1: return "ie";
          case agent.indexOf("firefox") > -1: return "firefox";
          case agent.indexOf("safari") > -1: return "safari";
          default: return "other";
        }
      })(window.navigator.userAgent.toLowerCase());

      // Init stripe once.
      $('body', context).once('commerce-payment-request-stripe', function () {
        // Change payment method depending on what is available. We default
        // to Google Pay.
        // Apple Pay.
        if (window.ApplePaySession) {
          $('#edit-commerce-payment-payment-method label[for="edit-commerce-payment-payment-method-commerce-stripe-payment-requestcommerce-payment-commerce-stripe-payment-request"]').text('Apple Pay');
        }
        // Microsoft Pay.
        else if (browser == 'ie') {
          $('#edit-commerce-payment-payment-method label[for="edit-commerce-payment-payment-method-commerce-stripe-payment-requestcommerce-payment-commerce-stripe-payment-request"]').text('Microsoft Pay');
        }
      });

      $('#stripe-payment-request-button', context).once('commerce-payment-request-stripe').click(function (ev) {
        ev.preventDefault();
      });

      // Check the availability of the Payment Request API first.
      var activePaymentType = $('input[name="commerce_payment[payment_method]"]:checked', context).val();
      if (typeof activePaymentType !== 'undefined') {
        var $form = $(Drupal.settings.checkout_form_id);

        // Check if Stripe Payment Request tab active.
        if (activePaymentType.indexOf('commerce_stripe_payment_request') >= 0) {
          // Create our Payment Request button.
          var stripe = Stripe(Drupal.settings.commerce_stripe_payment_request.stripe_key);
          var paymentRequest = stripe.paymentRequest(Drupal.settings.commerce_stripe_payment_request.paymentRequest);

          $form.once('commerce-payment-request-stripe', function () {
            var elements = stripe.elements();
            var prButton = elements.create('paymentRequestButton', {
              paymentRequest: paymentRequest,
              style: {
                paymentRequestButton: {
                  height: '44px',
                },
              },
            });

            // Check if we can make payments.
            paymentRequest.canMakePayment().then(function (result) {
              // If we can make Stripe Apple/Google payments, show the button.
              if (result) {
                Drupal.behaviors.commerceStripeWebPaymentRequest.showButton();
                prButton.mount('#stripe-payment-request-button');
              }
              // Else, if they can't make payments, show the setup info.
              else {
                Drupal.behaviors.commerceStripeWebPaymentRequest.showSetUpButton();
              }
            });

            // Listen for a token, process and complete the payment on our end.
            paymentRequest.on('token', function (ev) {
              // Send the token to the server to charge it!
              // Prepare data for backend call.
              var charge_data = {
                token: ev.token.id,
                result: ev,
                payment_method_id: Drupal.settings.commerce_stripe_payment_request.payment_method,
                payment_request: Drupal.settings.commerce_stripe_payment_request.paymentRequest,
                serialized_form: $(Drupal.settings.checkout_form_id).formSerialize()
              };

              var charge_path = Drupal.settings.basePath + Drupal.settings.pathPrefix + Drupal.settings.commerce_stripe_payment_request.charge_path;

              // Send the card details to the server so we can charge the card and
              // complete the payment.
              fetch(charge_path, {
                method: 'POST',
                body: JSON.stringify({data: charge_data}),
                headers: {'content-type': 'application/json'},
              })
                .then(function (response) {
                  if (response.ok) {
                    ev.complete('success');

                    // Examine the text in the response.
                    response.json().then(function(data) {
                      var alter_event = jQuery.Event('stripePaymentRequestPaySuccess');
                      alter_event.charge_data = charge_data;
                      alter_event.server_response = data;
                      $('#stripe-payment-request-button').trigger(alter_event);

                      // Redirect to complete page.
                      window.location.href = data.redirect_uri;
                    });
                  }
                  else {
                    ev.complete('fail');

                    // Examine the text in the response.
                    response.json().then(function(data) {
                      var alter_event = jQuery.Event('stripePaymentRequestPayError');
                      alter_event.error_message = data;
                      $('#stripe-payment-request-button').trigger(alter_event);
                    });
                  }
                });
            });
          });
        }
        // Hides buttons and removes class if Apple/Google Pay tab isn't active.
        else {
          Drupal.behaviors.commerceStripeWebPaymentRequest.hideButtons();
          $form.removeClass('stripe-commerce-payment-request-not-configured');
        }
      }
    },

    // Show our Apple/Google Pay button, if payments can be made.
    showButton: function () {
      $('.checkout-continue').hide();
      $('.stripe-payment-request-setup-wrapper').show();
      $('#stripe-payment-request-button').show();
    },

    // Show the setup info, if payments can't be made.
    showSetUpButton: function () {
      $('.checkout-continue').hide();
      // Shows wrapper and button, because Stripe hides Setup button by default.
      $('.stripe-payment-request-setup-wrapper').show();
      $('#stripe-payment-request-unavailable').show();
    },

    // Hide our Payment Request button if Stripe Payment Request method isn't active.
    hideButtons: function () {
      $('#stripe-payment-request-button').hide();
      $('.stripe-payment-request-setup-wrapper').hide();
      $('.checkout-continue').show();
    }
  };
})(jQuery);
